# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.14)
# Database: agriplex
# Generation Time: 2017-02-06 05:10:54 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table admin
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(48) NOT NULL DEFAULT '',
  `email` varchar(256) NOT NULL DEFAULT '',
  `password` varchar(64) NOT NULL DEFAULT '',
  `token` varchar(64) NOT NULL DEFAULT '',
  `role` varchar(15) NOT NULL DEFAULT 'admin' COMMENT 'admin,support,sales',
  `status` varchar(15) NOT NULL DEFAULT 'inactive' COMMENT 'active, inactive, suspended,blocked',
  `date_created` datetime DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_email` (`email`),
  KEY `idx_email` (`email`),
  KEY `idx_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;

INSERT INTO `admin` (`id`, `name`, `email`, `password`, `token`, `role`, `status`, `date_created`, `date_modified`)
VALUES
	(1,'Zainul Abdeen','pixces@yahoo.com','21232f297a57a5a743894a0e4a801fc3','usertoken','admin','active',NULL,'2016-10-29 17:06:48');

/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table attribute_group
# ------------------------------------------------------------

DROP TABLE IF EXISTS `attribute_group`;

CREATE TABLE `attribute_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(150) NOT NULL DEFAULT '',
  `label_seo` varchar(150) NOT NULL DEFAULT '',
  `sort_order` int(2) NOT NULL DEFAULT '0',
  `date_created` datetime DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `attribute_group` WRITE;
/*!40000 ALTER TABLE `attribute_group` DISABLE KEYS */;

INSERT INTO `attribute_group` (`id`, `label`, `label_seo`, `sort_order`, `date_created`, `date_modified`)
VALUES
	(1,'Fertilizers','fertilizers',0,NULL,'2017-02-05 12:13:13'),
	(2,'Pesticides','pesticides',0,NULL,'2017-02-05 12:13:21');

/*!40000 ALTER TABLE `attribute_group` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table attribute_group_attributes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `attribute_group_attributes`;

CREATE TABLE `attribute_group_attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_group_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `attribute_id` (`attribute_id`),
  KEY `attribute_group_id` (`attribute_group_id`),
  CONSTRAINT `attribute_group_attributes_ibfk_1` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`),
  CONSTRAINT `attribute_group_attributes_ibfk_2` FOREIGN KEY (`attribute_group_id`) REFERENCES `attribute_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `attribute_group_attributes` WRITE;
/*!40000 ALTER TABLE `attribute_group_attributes` DISABLE KEYS */;

INSERT INTO `attribute_group_attributes` (`id`, `attribute_group_id`, `attribute_id`, `date_created`, `date_modified`)
VALUES
	(1,1,1,NULL,'2017-02-05 12:17:13'),
	(2,1,2,NULL,'2017-02-05 12:17:16'),
	(3,1,3,NULL,'2017-02-05 12:17:19'),
	(4,1,4,NULL,'2017-02-05 12:17:21');

/*!40000 ALTER TABLE `attribute_group_attributes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table attribute_select_values
# ------------------------------------------------------------

DROP TABLE IF EXISTS `attribute_select_values`;

CREATE TABLE `attribute_select_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) DEFAULT NULL,
  `value` text,
  `date_cretaed` datetime DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `attribute_id` (`attribute_id`),
  CONSTRAINT `attribute_select_values_ibfk_1` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `attribute_select_values` WRITE;
/*!40000 ALTER TABLE `attribute_select_values` DISABLE KEYS */;

INSERT INTO `attribute_select_values` (`id`, `attribute_id`, `value`, `date_cretaed`, `date_modified`)
VALUES
	(1,2,'100 ml',NULL,'2017-02-05 12:29:19'),
	(2,2,'250 ml',NULL,'2017-02-05 12:29:31'),
	(3,2,'500 ml',NULL,'2017-02-05 12:29:40'),
	(4,2,'1 Ltr',NULL,'2017-02-05 12:29:49'),
	(5,2,'5 Ltr',NULL,'2017-02-05 12:30:02'),
	(6,2,'20 Ltr',NULL,'2017-02-05 12:30:10');

/*!40000 ALTER TABLE `attribute_select_values` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table attributes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `attributes`;

CREATE TABLE `attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(45) DEFAULT NULL,
  `attribute_code` varchar(45) DEFAULT NULL,
  `option_type_id` int(11) NOT NULL,
  `is_variance` int(1) NOT NULL DEFAULT '0',
  `is_multiselect` int(1) NOT NULL DEFAULT '0',
  `is_required` int(1) NOT NULL DEFAULT '0',
  `sort_order` int(2) NOT NULL DEFAULT '0',
  `date_cerated` datetime DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `option_type_id` (`option_type_id`),
  CONSTRAINT `attributes_ibfk_1` FOREIGN KEY (`option_type_id`) REFERENCES `option_types` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `attributes` WRITE;
/*!40000 ALTER TABLE `attributes` DISABLE KEYS */;

INSERT INTO `attributes` (`id`, `label`, `attribute_code`, `option_type_id`, `is_variance`, `is_multiselect`, `is_required`, `sort_order`, `date_cerated`, `date_modified`)
VALUES
	(1,'Dosage','dosage',7,0,0,0,0,NULL,'2017-02-05 11:53:54'),
	(2,'Packaging','packaging',3,1,0,1,0,NULL,'2017-02-05 12:16:13'),
	(3,'Expiry date','expiry_date',9,0,0,0,0,NULL,'2017-02-05 12:16:15'),
	(4,'Batch No','batch_no',7,0,0,0,0,NULL,'2017-02-05 12:16:18');

/*!40000 ALTER TABLE `attributes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL DEFAULT '',
  `seo_name` varchar(128) NOT NULL DEFAULT '',
  `parent` int(11) NOT NULL DEFAULT '0',
  `description` text,
  `meta_title` text,
  `meta_description` text,
  `meta_keywords` text,
  `image` varchar(256) DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  `is_searchable` int(1) DEFAULT '0',
  `on_navbar` int(11) NOT NULL DEFAULT '0',
  `status` varchar(15) NOT NULL DEFAULT 'active' COMMENT 'active,inactive,',
  `date_created` datetime DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;

INSERT INTO `categories` (`id`, `name`, `seo_name`, `parent`, `description`, `meta_title`, `meta_description`, `meta_keywords`, `image`, `order`, `is_searchable`, `on_navbar`, `status`, `date_created`, `date_modified`)
VALUES
	(1,'Sprayer & Adjuvant','sprayer_adjuvant',0,'','','','','',4,0,1,'inactive','2016-10-30 02:44:24','2016-10-30 05:03:24'),
	(2,'Sprayer','sprayer',1,'','','','','',1,0,1,'inactive','2016-10-30 02:44:24','2016-10-30 05:04:15'),
	(3,'Adjuvant','adjuvant',1,'','','','','',2,0,1,'active','2016-10-30 02:44:24','2016-10-30 17:54:45'),
	(4,'Fertilizers','fertilizers',0,'','','','','',2,0,1,'active','2016-10-30 02:44:24','2016-10-30 02:44:54'),
	(5,'Pesticides','pesticides',0,'','','','','',3,0,1,'inactive','2016-10-30 02:44:24','2016-10-30 05:00:52'),
	(6,'Seeds','seeds',0,'','','','','',1,0,1,'active','2016-10-30 02:44:24','2016-10-30 02:44:54'),
	(7,'Bio Activator','bio_activator',4,'','','','','',9,0,0,'active','2016-10-30 02:44:24','2016-10-30 02:44:54'),
	(8,'Plant Growth regulator','plant_growth_regulator',4,'','','','','',10,0,0,'active','2016-10-30 02:44:24','2016-10-30 02:44:54'),
	(9,'Organic Manure','organic_manure',4,'','','','','',11,0,0,'active','2016-10-30 02:44:24','2016-10-30 02:44:54'),
	(10,'Bio Fertilizers','bio_fertilizers',4,'','','','','',12,0,0,'active','2016-10-30 02:44:24','2016-10-30 02:44:54'),
	(11,'Bio Pesticides','bio_pesticides',5,'','','','','',5,0,0,'active','2016-10-30 02:44:24','2016-10-30 02:44:54'),
	(12,'Inceticide','incenticide',5,'','','','','',1,0,0,'active','2016-10-30 02:44:24','2016-10-30 02:44:54'),
	(13,'Fungicide','fungicide',5,'','','','','',2,0,0,'active','2016-10-30 02:44:24','2016-10-30 02:44:54'),
	(14,'Weedicide','weedicide',5,'','','','','',3,0,0,'active','2016-10-30 02:44:24','2016-10-30 02:44:54'),
	(15,'Bactericide','batericide',5,'','','','','',4,0,0,'active','2016-10-30 02:44:24','2016-10-30 02:44:54'),
	(16,'Major Nutrient','major_nutrient',4,'','','','','',1,0,0,'active','2016-10-30 02:44:24','2016-10-30 02:44:54'),
	(17,'Chelated Secondary nutrient','chelated_secondary_nutrient',4,'','','','','',2,0,0,'active','2016-10-30 02:44:24','2016-10-30 02:44:54'),
	(18,'Secondary nutrient','secondary_nutrient',4,'','','','','',3,0,0,'active','2016-10-30 02:44:24','2016-10-30 02:44:54'),
	(19,'Chelated Micronutrient','chelated_micronutrient',4,'','','','','',4,0,0,'active','2016-10-30 02:44:24','2016-10-30 02:44:54'),
	(20,'Non-chelatd Micronutrient','non_chelatd_micronutrient',4,'','','','','',5,0,0,'active','2016-10-30 02:44:24','2016-10-30 02:44:54'),
	(21,'Chelated multi-micronutrient mixtures','chelated_multi_micronutrient_mixtures',4,'','','','','',6,0,0,'active','2016-10-30 02:44:24','2016-10-30 02:44:54'),
	(22,'Non-chelated multi-micronutrient mixtures','non_chelated_multi_micronutrient_mixtures',4,'','','','','',7,0,0,'active','2016-10-30 02:44:24','2016-10-30 02:44:54'),
	(23,'Mineral mixture','mineral_mixture',4,'','','','','',8,0,0,'active','2016-10-30 02:44:24','2016-10-30 02:44:54');

/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table crop_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `crop_types`;

CREATE TABLE `crop_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(48) DEFAULT NULL,
  `seo_name` varchar(64) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `crop_types` WRITE;
/*!40000 ALTER TABLE `crop_types` DISABLE KEYS */;

INSERT INTO `crop_types` (`id`, `name`, `seo_name`, `date_created`, `date_modified`)
VALUES
	(1,'Food Grains','food_grains','2016-11-01 20:11:08','2017-02-04 12:05:13'),
	(2,'Vegetables','vegetables','2016-11-01 20:11:08','2017-02-04 12:05:13'),
	(3,'Fruits','fruits','2016-11-01 20:11:08','2017-02-04 12:05:14'),
	(4,'Flowers','flowers','2016-11-01 20:11:08','2017-02-04 12:05:15');

/*!40000 ALTER TABLE `crop_types` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table crops
# ------------------------------------------------------------

DROP TABLE IF EXISTS `crops`;

CREATE TABLE `crops` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `crop_type_id` int(11) NOT NULL,
  `name` varchar(48) NOT NULL DEFAULT '',
  `seo_name` varchar(64) NOT NULL DEFAULT '',
  `image` varchar(256) DEFAULT NULL,
  `icon` varchar(256) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_crop_type` (`crop_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `crops` WRITE;
/*!40000 ALTER TABLE `crops` DISABLE KEYS */;

INSERT INTO `crops` (`id`, `crop_type_id`, `name`, `seo_name`, `image`, `icon`, `date_created`, `date_modified`)
VALUES
	(1,1,'Wheat','wheat',NULL,NULL,'2016-11-01 20:13:28','2017-02-04 12:05:19'),
	(2,1,'Maize','maize',NULL,NULL,'2016-11-01 20:13:28','2017-02-04 12:05:20'),
	(3,1,'Cotton','cotton',NULL,NULL,'2016-11-01 20:13:28','2017-02-04 12:05:20'),
	(4,1,'Paddy','paddy',NULL,NULL,'2016-11-01 20:13:28','2017-02-04 12:05:21'),
	(5,2,'Potato','potato',NULL,NULL,'2016-11-01 20:13:28','2017-02-04 12:05:21'),
	(6,2,'Tomato','tomato',NULL,NULL,'2016-11-01 20:13:28','2017-02-04 12:05:22'),
	(7,2,'Onion','onion',NULL,NULL,'2016-11-01 20:13:28','2017-02-04 12:05:23'),
	(8,2,'Peas','peas',NULL,NULL,'2016-11-01 20:13:28','2017-02-04 12:05:23'),
	(9,2,'Brinjal','brinjal',NULL,NULL,'2016-11-01 20:13:28','2017-02-04 12:05:24'),
	(10,3,'Papaya','papaya',NULL,NULL,'2016-11-01 20:13:28','2017-02-04 12:05:24'),
	(11,3,'Banana','banana',NULL,NULL,'2016-11-01 20:13:28','2017-02-04 12:05:25'),
	(12,3,'Apple','apple',NULL,NULL,'2016-11-01 20:13:28','2017-02-04 12:05:25'),
	(13,3,'Mango','mango',NULL,NULL,'2016-11-01 20:13:28','2017-02-04 12:05:26'),
	(14,3,'Orange','orange',NULL,NULL,'2016-11-01 20:13:28','2017-02-04 12:05:26');

/*!40000 ALTER TABLE `crops` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table customer
# ------------------------------------------------------------

DROP TABLE IF EXISTS `customer`;

CREATE TABLE `customer` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `username` varchar(45) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table customer_address
# ------------------------------------------------------------

DROP TABLE IF EXISTS `customer_address`;

CREATE TABLE `customer_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table customer_profile
# ------------------------------------------------------------

DROP TABLE IF EXISTS `customer_profile`;

CREATE TABLE `customer_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table manufacturers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `manufacturers`;

CREATE TABLE `manufacturers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL DEFAULT '',
  `product_category` text,
  `description` text,
  `website` text,
  `image` text,
  `location` text,
  `discount` int(2) NOT NULL DEFAULT '0' COMMENT 'discount %',
  `sort_order` int(1) NOT NULL DEFAULT '0',
  `date_created` datetime DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `manufacturers` WRITE;
/*!40000 ALTER TABLE `manufacturers` DISABLE KEYS */;

INSERT INTO `manufacturers` (`id`, `name`, `product_category`, `description`, `website`, `image`, `location`, `discount`, `sort_order`, `date_created`, `date_modified`)
VALUES
	(1,'Bayer CropScience Ltd','Pesticides and Agro Chemical',NULL,'www.bayer.co.in',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:18:50'),
	(2,'PI Industries Ltd','Pesticides and Agro Chemical',NULL,'www.piindustries.com',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:11:25'),
	(3,'Dhanuka Agritech Ltd','Pesticides and Agro Chemical',NULL,'www.dhanuka.com',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:11:21'),
	(4,'Monsanto India Ltd','Pesticides and Agro Chemical',NULL,'www.monsanto.com',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(5,'Rallis India Ltd','Pesticides and Agro Chemical',NULL,'www.rallis.co.in',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(6,'Sharda Cropchem Ltd','Pesticides and Agro Chemical',NULL,'www.shardacropchem.com',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(7,'Excel Crop Care Ltd','Pesticides and Agro Chemical',NULL,'www.excelcropcare.com',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(8,'Insecticides India Ltd','Pesticides and Agro Chemical',NULL,'www.insecticidesindia.com',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(9,'Meghmani Organics Ltd','Pesticides and Agro Chemical',NULL,'www.meghmani.com',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(10,'Bharat Rasayan Ltd','Pesticides and Agro Chemical',NULL,'www.bharatrasayan.net',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(11,'FACT Engineering and Design Development Organisation (FEDO)','Fertilizers',NULL,'www.fedo.com',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(12,'Madras Fertilizers Limited','Fertilizers',NULL,'www.madrasfert.nic.in',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(13,'National Fertilizers Limited','Fertilizers',NULL,'www.nationalfertilizers.com',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(14,'Paradeep Phosphates Limited','Fertilizers',NULL,'www.paradeepphosphates.com',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(15,'Pyrites, Phosphates & Chemicals Limited','Fertilizers',NULL,'www.ppclindia.com',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(16,'Rashtriya Chemicals & Fertilizers Limited','Fertilizers',NULL,'www.rcfltd.com',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(17,'Indian Farmers Fertiliser Cooperative Ltd.','Fertilizers',NULL,'www.iffco.nic.in',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(18,'Krishak Bharati Cooperative Limited','Fertilizers',NULL,'www.kribhco.org',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(19,'Chambal Fertilizers & Chemicals Limited','Fertilizers',NULL,'www.zuari-chambal.com',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(20,'Coromondel Fertilisers Limited','Fertilizers',NULL,'www.cflindia.com',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(21,'Duncans Industries Limited','Fertilizers',NULL,'www.duncansfertiliser.com',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(22,'Deepak Fertilizers & Petrochemicals Ltd.','Fertilizers',NULL,'www.dfpcl.com',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(23,'Godavari Fertilizers & Chemicals Limited','Fertilizers',NULL,'www.gfcl.co.in',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(24,'Gujarat Narmada Valley Fertilisers Co. Ltd.','Fertilizers',NULL,'www.gnvfc.com',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(25,'Gujarat State Fertilizer Company Limited','Fertilizers',NULL,'www.gsfclimited.com',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(26,'Indo-Gulf Fertlizers & Chemicals Corpn. Ltd.','Fertilizers',NULL,'www.indo-gulf.com',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(27,'Mangalore Chemicals & Fertilizers Limited','Fertilizers',NULL,'www.mangalorechemicals.com',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(28,'Nagarjuna Fertilizers & Chemicals Limited','Fertilizers',NULL,'www.nagarjunagroup.com',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(29,'Shriram Fertilisers & Chemicals Limited','Fertilizers',NULL,'www.dscl.com',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(30,'Southern Petrochemicals Industries Corpn. Ltd.','Fertilizers',NULL,'www.spic.in',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(31,'Tata Chemicals Limited','Fertilizers',NULL,'www.tata.com',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(32,'Oswal Chemicals & Fertilizers Limited','Fertilizers',NULL,'www.oswalfert.com',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(33,'The Dharamsi Morarji Chemical Company Limited','Fertilizers',NULL,'www.dmcc.com',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(34,'Coromandel Int','Fertilizers',NULL,'',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(35,'GSFC','Fertilizers',NULL,'',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(36,'Rashtriya Chem','Fertilizers',NULL,'',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(37,'Chambal Fert','Fertilizers',NULL,'',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(38,'Fert and Chem','Fertilizers',NULL,'',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(39,'NFL','Fertilizers',NULL,'',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(40,'GNFC','Fertilizers',NULL,'',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(41,'Deepak Fert','Fertilizers',NULL,'',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(42,'Zuari Agro Chem','Fertilizers',NULL,'',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(43,'Mangalore Chem','Fertilizers',NULL,'',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(44,'SPIC','Fertilizers',NULL,'',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(45,'Zuari Global','Fertilizers',NULL,'',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(46,'JK Agri Genetic','Fertilizers',NULL,'',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(47,'Gloster','Fertilizers',NULL,'',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(48,'Khaitan Chem','Fertilizers',NULL,'',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(49,'Bharat Agri','Fertilizers',NULL,'',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(50,'Basant Agro Tec','Fertilizers',NULL,'',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(51,'Rama Phosphates','Fertilizers',NULL,'',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(52,'Dharamsi Morarj','Fertilizers',NULL,'',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(53,'Shiva Global','Fertilizers',NULL,'',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(54,'Teesta Agro Ind','Fertilizers',NULL,'',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(55,'Agri-Tech','Fertilizers',NULL,'',NULL,NULL,0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(56,'COROMANDEL INTERNATIONAL LIMITED','Fertilizers',NULL,'',NULL,'SECUNDERABAD,  ANDHRA PRADESH',0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(57,'NATIONAL FERTILIZERS LTD.','Fertilizers',NULL,'',NULL,'NOIDA, UTTAR PRADESH',0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(58,'CHAMBAL FERTILIZERS & CHEMICALS LTD.','Fertilizers',NULL,'',NULL,'NEW DELHI',0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(59,'GUJARAT STATE FERTILIZERS & CHEMICALS LTD.','Fertilizers',NULL,'',NULL,'VADODARA',0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(60,'RCF LTD.','Fertilizers',NULL,'',NULL,'MUMBAI',0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(61,'FACT','Fertilizers',NULL,'',NULL,'KOCHIN',0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(62,'DEEPAK FERTILIZERS & PETROCHEMICALS CORPORATION LTD.','Fertilizers',NULL,'',NULL,'PUNE, MAHARASHTRA',0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(63,'GUJARAT NARMADA VALLEY FERTILIZERS & CHEMICALS LTD.','Fertilizers',NULL,'',NULL,'BHARUCH',0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(64,'ZUARI AGRO CHEMICALS','Fertilizers',NULL,'',NULL,'GOA',0,0,'2017-02-05 00:09:48','2017-02-05 00:09:48'),
	(65,'Multiplex','Fertilizers	and Agro products',NULL,NULL,NULL,'Bangalore',0,0,NULL,'2017-02-05 12:06:21');

/*!40000 ALTER TABLE `manufacturers` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table option_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `option_types`;

CREATE TABLE `option_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(45) DEFAULT NULL,
  `has_multiple_values` int(1) NOT NULL DEFAULT '0',
  `sort_order` int(2) NOT NULL DEFAULT '0',
  `date_created` date DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `option_types` WRITE;
/*!40000 ALTER TABLE `option_types` DISABLE KEYS */;

INSERT INTO `option_types` (`id`, `label`, `has_multiple_values`, `sort_order`, `date_created`, `date_modified`)
VALUES
	(1,'checkbox',1,0,NULL,'2017-02-04 12:54:30'),
	(2,'radio',1,0,NULL,'2017-02-04 12:54:28'),
	(3,'select',1,0,NULL,'2017-02-04 12:54:34'),
	(4,'color_swatch',1,0,NULL,'2017-02-04 12:54:34'),
	(5,'text_swatch',1,0,NULL,'2017-02-04 12:54:35'),
	(6,'image_swatch',1,0,NULL,'2017-02-04 12:54:36'),
	(7,'text',0,0,NULL,'2017-02-04 12:54:09'),
	(8,'textarea',0,0,NULL,'2017-02-04 12:54:09'),
	(9,'date',0,0,NULL,'2017-02-04 12:54:09'),
	(10,'date_time',0,0,NULL,'2017-02-04 12:54:09');

/*!40000 ALTER TABLE `option_types` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pincode
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pincode`;

CREATE TABLE `pincode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table product_attributes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_attributes`;

CREATE TABLE `product_attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `type` varchar(15) NOT NULL DEFAULT '' COMMENT 'int, text, select - refers to attribute_select_values id',
  `value` varchar(200) NOT NULL DEFAULT '',
  `date_created` datetime DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `attribute_id` (`attribute_id`),
  CONSTRAINT `product_attributes_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  CONSTRAINT `product_attributes_ibfk_2` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table product_descriptions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_descriptions`;

CREATE TABLE `product_descriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `name` varchar(11) DEFAULT NULL,
  `name_seo` varchar(11) DEFAULT NULL,
  `description` longtext,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` longtext,
  `meta_keywords` text,
  `tags` text,
  `date_created` datetime DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `product_descriptions_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table product_images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_images`;

CREATE TABLE `product_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `image` text NOT NULL,
  `is_processed` int(1) NOT NULL DEFAULT '0',
  `status` varchar(15) NOT NULL DEFAULT 'pending',
  `sort_order` int(2) NOT NULL DEFAULT '0',
  `date_created` datetime DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `product_images_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table product_price
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_price`;

CREATE TABLE `product_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '0',
  `price` decimal(7,2) NOT NULL,
  `discount` decimal(3,2) NOT NULL DEFAULT '0.00',
  `effective_price` decimal(7,2) NOT NULL,
  `order_quantity` int(11) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `product_price_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table product_related
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_related`;

CREATE TABLE `product_related` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table product_relations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_relations`;

CREATE TABLE `product_relations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `child_id` int(11) NOT NULL DEFAULT '0',
  `date_created` datetime DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `parent_id` (`parent_id`),
  KEY `child_id` (`child_id`),
  CONSTRAINT `product_relations_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  CONSTRAINT `product_relations_ibfk_2` FOREIGN KEY (`parent_id`) REFERENCES `products` (`id`),
  CONSTRAINT `product_relations_ibfk_3` FOREIGN KEY (`child_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `attribute_group_id` int(11) NOT NULL,
  `sku` varchar(45) DEFAULT NULL,
  `ean` varchar(45) DEFAULT NULL,
  `product_type_id` varchar(25) NOT NULL DEFAULT 'simple' COMMENT 'Simple, configurable, Combo',
  `manufacturer_id` int(11) NOT NULL,
  `has_options` int(1) NOT NULL DEFAULT '0',
  `is_hidden` int(1) NOT NULL DEFAULT '0',
  `required_options` int(1) NOT NULL DEFAULT '0',
  `shipping_charges` decimal(7,2) DEFAULT NULL,
  `out_of_stock` int(1) NOT NULL DEFAULT '0' COMMENT '0, 1',
  `status` varchar(15) NOT NULL DEFAULT 'active' COMMENT 'active, inactive',
  `date_created` datetime DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_category` (`category_id`),
  KEY `fk_manufacturer` (`manufacturer_id`),
  KEY `fk_attribute_group_id` (`attribute_group_id`),
  CONSTRAINT `fk_attribute_group_id` FOREIGN KEY (`attribute_group_id`) REFERENCES `attribute_group` (`id`),
  CONSTRAINT `fk_category` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_manufacturer` FOREIGN KEY (`manufacturer_id`) REFERENCES `manufacturers` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `object_id` int(10) unsigned NOT NULL,
  `object_type` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  UNIQUE KEY `object_id` (`object_id`,`object_type`,`key`),
  KEY `object_id_2` (`object_id`,`object_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;

INSERT INTO `settings` (`object_id`, `object_type`, `key`, `value`, `created_at`)
VALUES
	(1,'user','cookie_token','9vQMLdfDKghczV8nCwuTVK0GCXx2ysozzKyGXKov','2017-02-05 12:38:20');

/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `email`, `password`, `name`, `created_at`)
VALUES
	(1,'test@example.org','$2a$10$4wtMMI613pcGWVzLlIFEbOCW/ni5RsSFoJ7Ygi6P18cc51gO4CUTC','Johnny Test','2017-02-05 17:43:39');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
