<?php
//defined('BASE_PATH') || define('BASE_PATH', getenv('BASE_PATH') ?: realpath(dirname(__FILE__) . '/../..'));
//defined('APP_PATH') || define('APP_PATH', BASE_PATH . '/app');

return [
    'app' => [
        'debug'         => true,
        'responseMode'  => 'view',
        'environment'   => 'local',
        'modules'       => [
                'api'   => 'Api'
        ],
        'profiling'     => true,
    ],
    'path' => [
        'hostname'      => 'http://localhost/agriplex/',
        'baseUri'       => 'http://localhost/agriplex/',
        'assetUri'      => 'http://localhost/agriplex/public/',
        /* Logger */
        'logger'          => [
            'adapter'   => 'File',
            'path'      => ROOT_PATH . '/var/logs/',
            'appFile'   => 'application',
            'cliFile'   => 'cli',
            'db'        => 'db',
            'format'    => '[%date%][%type%] %message%',
            'rotation'  => true
        ],
    ],
    'database' => [
        'adapter'       => 'Mysql',
        'host'          => 'localhost',
        'username'      => 'root',
        'password'      => 'root',
        'dbname'        => 'agriplex',
        'charset'       => 'utf8',
        'persistent'    => true
    ],
    'application' => [
        'appDir'            => APP_PATH . '/',
        'controllersDir'    => APP_PATH . '/controllers/',
        'modelsDir'         => APP_PATH . '/models/',
        'migrationsDir'     => APP_PATH . '/migrations/',
        'viewsDir'          => APP_PATH . '/views/',
        'pluginsDir'        => APP_PATH . '/plugins/',
        'libraryDir'        => APP_PATH . '/library/',
        'componentsDir'     => APP_PATH . '/library/components/',
        'cacheDir'          => ROOT_PATH . '/cache/',
        'baseUri'           => '/agriplex/',
    ],
    'session' => [
        // can be 'redis' or 'files'
        'adapter' => 'files',
        'name' => 'phalcon',
        'lifetime' => 1440,
        'cookieLifetime' => 86400 ],

    'cache' => [
        // can be 'redis' or 'files'
        'adapter' => 'files',
        'prefix' => '',
        // only used for files adapter
        // should have web user group write
        'dir' => ROOT_PATH . '/var/cache/'
    ],
    'cookies' => [
        // 14 days
        'expire' => 60*60*24*14,
        'path' => '/',
        'secure' => TRUE,
        'httpOnly' => TRUE ],
    'redis' => [
        'cache' => [
            'host' => 'localhost',
            'port' => 6379 ],
        'session' => [
            'host' => 'localhost',
            'port' => 6379,
            'prefix' => 'session:' ]
    ],
    'mongodb' => [
        'host' => 'localhost',
        'port' => 27017,
        'username' => '',
        'password' => '',
        'dbname' => 'phalcon' ],

    'profiling' => [
        'system' => TRUE,
        'query' => TRUE ],

    'settings' => [
        'cookieToken' => 'cookie_token'
    ]
];
