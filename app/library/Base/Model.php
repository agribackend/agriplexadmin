<?php

/**
 * Created by PhpStorm.
 * User: zainulabdeen
 * Date: 2/6/17
 * Time: 12:06 AM
 */
namespace Agriplex\Library\Base;

use Phalcon\DI as DI;

class Model extends \Phalcon\Mvc\Model
{
    protected $behaviors = array();

    function getService( $service )
    {
        return $this->getDI()->get( $service );
    }

    static function getStaticService( $service )
    {
        return DI::getDefault()->get( $service );
    }

    static function getStaticDI()
    {
        return DI::getDefault();
    }
}
