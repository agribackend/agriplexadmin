<?php

/**
 * Created by PhpStorm.
 * User: zainulabdeen
 * Date: 2/6/17
 * Time: 12:05 AM
 */
namespace Agriplex\Library\Base;

use Phalcon\Di;
use Phalcon\Mvc\User\Component;

class Action extends Component
{
    function getService( $service )
    {
        return $this->getDI()->get( $service );
    }

    static function getStaticService( $service )
    {
        return Di::getDefault()->get( $service );
    }

}