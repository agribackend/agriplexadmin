<?php

/**
 * Created by PhpStorm.
 * User: zainulabdeen
 * Date: 2/5/17
 * Time: 11:34 PM
 */
namespace Agriplex\Library\Bootstrap;

use Agriplex\Library\Components\Auth,
    Agriplex\Library\Components\Cache,
    Agriplex\Library\Components\Mail,
    Agriplex\Library\Components\Util,
    Agriplex\Library\Components\Validate;
use Phalcon\Cache\Frontend\Data,
    Phalcon\Config,
    Phalcon\Db\Adapter\Pdo\Mysql,
    Phalcon\Db\Profiler,
    Phalcon\Di,
    Phalcon\Di\FactoryDefault,
    Phalcon\Events\Manager,
    Phalcon\Http\Response\Cookies,
    Phalcon\Loader,
    Phalcon\Logger\Adapter\File,
    Phalcon\Logger\Formatter\Line,
    Phalcon\Mvc\Url,
    Phalcon\Session\Adapter\Files,
    Phalcon\Session\Adapter\Mongo,
    Phalcon\Session\Adapter\Redis,
    Phalcon\Mvc\Collection\Manager as CollectionManager,
    Phalcon\Mvc\Application,
    Phalcon\Mvc\Dispatcher,
    Phalcon\Mvc\View;

Abstract class Base
{
    /**
     * @var
     */
    protected $di;

    /**
     * @var
     */
    protected $services;

    /**
     * @var
     */
    protected $config;

    public function __construct( $services = array()){

        $this->di = new FactoryDefault();
        $this->services = $services;

    }

    public function run( $args = array() ){

        //initialize mandatory services
        $this->initConfig();
        $this->initLoader();

        foreach($this->services as $service){
            $function = 'init' .  ucfirst($service);
            $this->$function();
        }
        Di::setDefault( $this->di );
    }


    public function getDi(){
        return $this->di;
    }

    protected function initConfig(){

        //fetch the configuratation
        $defaultConfig = require_once APP_PATH . '/config/config.php';
        $localConfig = require_once APP_PATH . '/config/config.local.php';

        $config = new Config( $defaultConfig );
        $config->merge( new Config($localConfig) );

        $this->config = $config;
        $this->di->set('config',$config);
    }

    protected function initLoader(){

        $loader = new Loader();
        $loader->registerNamespaces([
            'Agriplex\Library\Components'   => APP_PATH .'/library/components/',
            'Agriplex\Library\Base'         => APP_PATH .'/library/Base/',
            'Agriplex\Controllers'          => APP_PATH .'/controllers/',
            'Agriplex\Models'               => APP_PATH .'/models/',
            'Agriplex\Library'              => APP_PATH .'/library/'
        ]);
        $loader->registerClasses([
            '__' => VENDOR_PATH .'/underscore/Underscore.php'
        ]);
        $loader->register();

        // autoload vendor dependencies
        require_once VENDOR_PATH .'/autoload.php';

        $this->di[ 'loader' ] = $loader;

    }

    protected function initRouter(){

        $config = $this->di[ 'config' ];
        $this->di->set(
            'router',
            function() use ($config){
                return require APP_PATH . '/config/routes.php';
            },
            TRUE
        );
    }

    protected function initView(){

        $config = $this->di[ 'config' ];

        $this->di->set(
            'view',
            function () use ( $config ) {
                $view = new View();
                $view->setViewsDir( APP_PATH .'/views/' );
                return $view;
            },
            TRUE );
    }

    protected function initDispatcher(){
        $eventsManager = $this->di[ 'eventsManager' ];

        $this->di->set(
            'dispatcher',
            function () use ( $eventsManager ) {
                // create the default namespace
                $dispatcher = new Dispatcher();
                $dispatcher->setDefaultNamespace( 'Agriplex\Controllers' );

                // set up our error handler
                $eventsManager->attach(
                    'dispatch:beforeException',
                    function ( $event, $dispatcher, $exception ) {
                        switch ( $exception->getCode() )
                        {
                            case Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
                            case Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
                                $dispatcher->forward([
                                    'namespace' => 'Agriplex\Controllers',
                                    'controller' => 'error',
                                    'action' => 'show404'
                                ]);
                                return FALSE;
                            default:
                                $dispatcher->forward([
                                    'namespace' => 'Agriplex\Controllers',
                                    'controller' => 'error',
                                    'action' => 'show500',
                                    'params' => [
                                        NULL, // responseMode
                                        $exception ]
                                ]);
                                return FALSE;
                        }
                    });

                $dispatcher->setEventsManager( $eventsManager );

                return $dispatcher;
            },
            TRUE );
    }

    protected function initLogger(){

        $config = $this->di->get('config');

        $this->di->set(
            'logger',
            function() use ($config){
                $fileName = $config->path->logger->path . $config->path->logger->appFile;
                if ($config->path->logger->rotation){
                    $fileName .= '-' . date('Ymd') . '.log';
                } else {
                    $fileName .= '.log';
                }

                $logger = new File($fileName);
                $formatter = new Line($config->path->logger->format);
                $logger->setFormatter($formatter);
                return $logger;
            },
            TRUE
        );

    }

    protected function initProfiler(){
        $this->di->set(
            'profiler',
            function () {
                return new Profiler();
            },
            TRUE );
    }

    protected function initDatabase(){
        $config = $this->di[ 'config' ];
        $profiler = $this->di[ 'profiler' ];
        $logger = $this->di['logger'];

        $this->di->set(
            'db',
            function () use ( $config, $profiler,$logger ) {
                // set up the database adapter
                $connection = new Mysql([
                    'host' => $config->database->host,
                    'username' => $config->database->username,
                    'password' => $config->database->password,
                    'dbname' => $config->database->dbname,
                    'persistent' => $config->database->persistent,
                    'options'  => [
                        \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES ' . $config->database->charset
                    ]
                ]);

                if ( $config->app->profiling ) {
                    $eventsManager = new Manager();

                    // listen to all the database events
                    $eventsManager->attach(
                        'db',
                        function ($event, $connection) use ($profiler) {
                            if ($event->getType() == 'beforeQuery'):
                                $profiler->startProfile($connection->getSQLStatement());
                            endif;

                            if ($event->getType() == 'afterQuery'):
                                $profiler->stopProfile();
                            endif;
                        });
                    $connection->setEventsManager($eventsManager);
                }

                //log the queries if debug is active
                if ($config->app->debug){
                    $eventsManager = new Manager();

                    $eventsManager->attach(
                        'db',
                        function ($event, $connection) use ($logger) {
                            if ($event->getType() == 'beforeQuery') {
                                $variables = $connection->getSqlVariables();
                                if ($variables){
                                    $logger->log($connection->getSQLStatement() . ' [' . join(',', $variables) . ']', PhLogger::INFO);
                                } else {
                                    $logger->log( $connection->getSQLStatement(), PhLogger::INFO );
                                }
                            }
                        });
                    $connection->setEventsManager($eventsManager);

                }
                return $connection;
            },
            TRUE );
    }

    protected function initUrl(){

        $config = $this->di[ 'config' ];

        $this->di->set(
            'url',
            function () use ( $config ) {
                $url = new Url();
                $url->setBaseUri( $config->paths->baseUri );
                $url->setStaticBaseUri( $config->paths->assetUri );
                return $url;
            },
            TRUE );
    }

    protected function initCookies(){
        $this->di->set(
            'cookies',
            function() {
                $cookies = new Cookies();
                $cookies->useEncryption( FALSE );
                return $cookies;
            },
            TRUE );
    }

    protected function initSession(){
        $config = $this->di[ 'config' ];

        $this->di->set(
            'session',
            function () use ( $config ) {
                if ( $config->session->adapter === 'redis' ):
                    $session = new Redis([
                        'path' => sprintf(
                            'tcp://%s:%s?weight=1&prefix=%s',
                            $config->redis->session->host,
                            $config->redis->session->port,
                            $config->redis->session->prefix ),
                        'name' => $config->session->name,
                        'lifetime' => $config->session->lifetime,
                        'cookie_lifetime' => $config->session->cookieLifetime
                    ]);
                else:
                    $session = new Files();
                endif;

                $session->start();
                return $session;
            },
            TRUE );
    }

    protected function initMongo()
    {
        $config = $this->di[ 'config' ];

        $this->di->set(
            'mongo',
            function () use ( $config ) {
                $mongo = new Mongo();
                return $mongo->selectDb(
                    $config->mongodb->dbname );
            },
            TRUE );
    }

    protected function initCollectionManager()
    {
        $this->di->set(
            'collectionManager',
            function(){
                return new CollectionManager();
            },
            TRUE );
    }

    protected function initDataCache(){
        $config = $this->di[ 'config' ];

        $this->di->set(
            'dataCache',
            function () use ( $config ) {
                // create a Data frontend and set a default lifetime to 1 hour
                $frontend = new Data([
                    'lifetime' => 3600
                ]);

                if ( $config->session->adapter === 'redis' ):
                    // connect to redis
                    $redis = new Redis();
                    $redis->connect(
                        $config->redis->cache->host,
                        $config->redis->cache->port );

                    // create the cache passing the connection
                    return new \Phalcon\Cache\Backend\Redis(
                        $frontend, [
                        'redis' => $redis
                    ]);
                else:
                    return new \Phalcon\Cache\Backend\File(
                        $frontend, [
                        'cacheDir' => $config->cache->dir,
                        'prefix' => $config->cache->prefix
                    ]);
                endif;
            },
            TRUE );
    }

    protected function initUtil(){

        $this->di->set(
            'util',
            function(){
                return new Util();
            },
            true
        );
    }

    protected function initAuth(){
        $this->di->set(
            'util',
            function(){
                return new Auth();
            },
            true
        );

    }

    protected function initValidate(){
        $this->di->set(
            'util',
            function(){
                return new Validate();
            },
            true
        );
    }

    protected function initCache(){
        $this->di->set(
            'util',
            function(){
                return new Cache();
            },
            true
        );
    }

    protected function initMail(){
        $this->di->set(
            'util',
            function(){
                return new Mail();
            },
            true
        );
    }
}