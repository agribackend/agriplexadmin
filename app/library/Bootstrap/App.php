<?php

/**
 * Created by PhpStorm.
 * User: zainulabdeen
 * Date: 2/5/17
 * Time: 11:34 PM
 */
namespace Agriplex\Library\Bootstrap;

use Phalcon\Loader;
use Agriplex\Library\Bootstrap\Base;
use Phalcon\Mvc\Application;

class App extends Base
{
    public function __construct($services = array())
    {
        parent::__construct($services);
    }

    public function run( $args = array() )
    {
        parent::run( $args );

        // initialize our benchmarks
        //$this->di[ 'util' ]->startBenchmark();

        // create the mvc application
        $application = new Application( $this->di );

        // run auth init
        //$this->di[ 'auth' ]->init();

        // output the content. our benchmark is finished in the base
        // controller before output is sent.
        echo $application->handle()->getContent();

    }

    protected function initConfig()
    {
        parent::initConfig();

        $config = $this->di['config'];

        if ($config->app->debug){
            error_reporting( E_ALL );
            ini_set( 'display_errors', 1);
        } else {
            error_reporting( 0 );
            ini_set( 'display_errors', 0);
        }

    }

    protected function initLoader()
    {
        parent::initLoader();

        $loader = new Loader();
        $loader->registerNamespaces([
            'Agriplex\Library\Components'   => APP_PATH .'/library/components/',
            'Agriplex\Library\Base'         => APP_PATH .'/library/Base/',
            'Agriplex\Controllers'          => APP_PATH .'/controllers/',
            'Agriplex\Models'               => APP_PATH .'/models/',
            'Agriplex\Library'              => APP_PATH .'/library/'
        ]);
        $loader->registerClasses([
            '__' => VENDOR_PATH .'/underscore/Underscore.php'
        ]);
        $loader->register();

        // autoload vendor dependencies
        require_once VENDOR_PATH .'/autoload.php';

        $this->di[ 'loader' ] = $loader;
    }


}