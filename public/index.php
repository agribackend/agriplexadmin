<?php
use Phalcon\Di\FactoryDefault;

error_reporting(E_ALL);


define('ROOT_PATH', dirname(__DIR__));
define('APP_PATH', ROOT_PATH. '/app');
define('VENDOR_PATH', ROOT_PATH . '/vendor' );

try {
    include_once APP_PATH . '/etc/constants.php';
    include_once APP_PATH . '/etc/helpers.php';
    include_once APP_PATH . '/library/Bootstrap/Base.php';
    include_once APP_PATH . '/library/Bootstrap/App.php';

    $bootstrap = new Agriplex\Library\Bootstrap\App(
        [
            'router','url','cookies','session','profiler','logger','database','collectionManager',
            'dataCache','view','dispatcher','util','auth','validate','cache','mail'
        ]
    );
    $bootstrap->run();

} catch (\Exception $e) {
    echo "Exception: ". $e->getMessage() . '<br>';
    echo nl2br( htmlentities($e->getTraceAsString()) );
}
